/**
 * Created by Anam on 1/25/14.
 */
//$(document).ready(function(){
//
//});
//localStorage['serviceURL'] = 'http://127.0.0.1/api/';
////localStorage['serviceURL'] = "http://coenraets.org/apps/directory/services/";
//var serviceURL = localStorage['serviceURL'];
////var serviceURL = 'http://127.0.0.1/directory2/services/';

//var scroll = new iScroll('wraper', { vScrollbar: true, vScroll: true, hScrollbar:false, hScroll: false});
var myScroll;
var pageItems;
var bnrUrls;
var category = getParam("cat").toUpperCase();
$(window).load(function() {
    $(document).ready(function() {
        $('.imgLabel').html(category);
    });
    setTimeout(getCatItems, 100);
});

//$(document).ajaxError(function(event, request, settings) {
//    $('#busy').hide();
//    alert("Error accessing the server");
//});


function getCatItems(){
//    $('#busy').show();
    $.ajax({
        type: "GET",
//        url: "pages/latestNews.json" ,
        url: "http://mycanvasworld.com/api/canvas_api.php?method=getLatestNews&count=15" ,
        dataType: "JSONp",
        timeout: 10000,
        success: function(json){
            newsItems = json;
            console.log(newsItems);
            $("#tickerNews li").remove();
            $.each(newsItems, function(index, news) {
                $('#tickerNews').append('<li class="ticker"><a href="../pages/articleDetails2.html?id=' + news.nwsId + '">' + news.title + '</a></li>');
            });
        }
    });

    $.ajax({
        type: "GET",
//        url: "sampleData.json" ,
//        url: "http://mycanvasworld.com/api/canvas_api.php?method=getAllPosts&category=" + category + "&count=15",
        url: "http://mycanvasworld.com/api/canvas_api.php?method=getAllPosts&category=" + category + "&count=15&post_from=2014-05-01&post_to=2014-05-31",
//                url: serviceURL + "canvas_api.php?dataType=posts&categories=" + category.toUpperCase()  ,
        dataType: "JSONp",
        success: function(json) {
//            $('#busy').hide();
            $("#catNewsList li").remove();
            pageItems = json;
            $.each(pageItems, function(index, news) {
                console.log(news.postId);
                $('#catNewsList').append('<li class="list-group-item"><a href="articleDetails2.html?id=' + news.postId + '">' +
                    '<p>' + news.title + '</p>' +
//                    '<p class="line2">' + news.detail + '<span style="color: #777777">&nbsp;বিস্তারিত >></span></p>' +
                    '</a></li>');
            });
            setTimeout(function(){
                myScroll.refresh();
            });
        },
        error: function(json){
            $('#catNewsList').append('<li>There was an error loading data</li>');
        }
    });

    $.ajax({
        type: "GET",
        url: "catBanner.json",
        dataType: "json",
        success: function(json){
            bnrUrls = json;
            console.log(bnrUrls);
            $('#bannerList li').remove();
            $.each(bnrUrls, function(index, items) {
                $('#bannerList').append('<li class="bnrImg"><a href="articleDetails2.html?id="' + items.id + '>' + '<img class="img-responsive" src="' + items.url + '">' + '</a></li>');
            });
        }
    });

    var count = 1;
    setInterval(function() {
        count = ($("#tickerNews").find(".ticker:nth-child("+count+")").fadeOut("slow").next().length == 0) ? 1 : count+1;
        $("#tickerNews").find(".ticker:nth-child("+count+")").fadeIn("slow");
    }, 3500);

    var cnt = 1;
    setInterval(function() {
        cnt = ($("#bannerList").find(".bnrImg:nth-child("+cnt+")").fadeOut("slow").next().length == 0) ? 1 : cnt+1;
        $("#bannerList").find(".bnrImg:nth-child("+cnt+")").fadeIn("slow");
    }, 5000);
}


function loaded () {
    myScroll = new IScroll('#wrapper',{ hScrollbar: false, vScrollbar: false });
}

document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', loaded, false);

//function loaded() {
//    setTimeout(function () {
//        myScroll = new iScroll('listWrapper',{ hScrollbar: false, vScrollbar: false });
//    }, 100);
//}
//window.addEventListener('load', loaded, false);
