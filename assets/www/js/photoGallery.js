/**
 * Created by Reverie on 5/14/14.
 */

var galleryItems;

$(document).ready(function() {
    document.addEventListener("deviceready", onDeviceReady(), false);
});

function onDeviceReady(){
    setTimeout(readJSON, 100);
}

function readJSON(){
    $.ajax({
        type: "GET",
        url: "galleryImgs.json" ,
//        url: "http://mycanvasworld.com/api/canvas_api.php?method=photoGallery" ,
        dataType: "json",
        timeout: 10000,
        success: function(json){
            galleryItems = json;
            console.log(galleryItems);
            $("#photoDiv img").remove();
            $.each(galleryItems, function(index, items) {
                $('#photoDiv').append('<img src="' + items.url + '" data-title="' + items.title + '" data-description="' + items.desc + '">');
            });
        },

        error: function(json){
            $('#photoDiv').text('There was an error loading data');
            console.log(json);
        }
    });
}