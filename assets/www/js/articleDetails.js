/**
 * Created by Anam on 2/12/14.
 */
//var serviceURL = localStorage['serviceURL'];
//var scroll = new iScroll('listWrapper', { vScrollbar: true, vScroll: true, hScrollbar:false, hScroll: false });

var myScroll;
var contentStr;
var idTrue;
var id = getParam("id");
var category = getParam("cat");
var detailItems;

$(window).load(function() {
    setTimeout(getDetails, 100);
});

if(id != "") idTrue = true;
if(category != "") idTrue = false;

$(document).ajaxError(function(event, request, settings) {
//    $('#busy').hide();
    alert("Error accessing the server");
});

function getDetails() {
//    $('#busy').show();
    if(idTrue == true) {
        $.ajax({
            type: "GET",
    //        url: "detailsData.json" ,
            url: "http://mycanvasworld.com/api/canvas_api.php?method=getPostDetail&post_id=" + id,
            dataType: "JSONp",
            success: function(json) {
    //            $('#busy').hide();
                detailItems = json;
                console.log(id);
                $('#textTitle').append(detailItems[0].title);
                $('#articleBody').append(detailItems[0].detail);
                $('#articleBody a > img').addClass("img-responsive");

    //            contentStr = JSON.stringify(detailItems[0].detail);
    //            console.log(contentStr);

                console.log(detailItems[0].detail);
    //            + '<p><button onclick="sharePost">Share</button></p>');

                setTimeout(function(){
                    myScroll.refresh();
                });
            },
            error: function(json){
                $('#articleBody').text('There was an error loading data');
            }
        });
    }

    if(idTrue == false) {
        $.ajax({
            type: "GET",
            //        url: "detailsData.json" ,
            url: "http://mycanvasworld.com/api/canvas_api.php?method=getAllPosts&category=" + category.toUpperCase(),
            dataType: "JSONp",
            success: function(json) {
                //            $('#busy').hide();
                detailItems = json;
                console.log(id);
                $('#textTitle').append("HOROSCOPE");
                $('#articleBody').append(detailItems[0].detail);
                $('#articleBody a > img').addClass("img-responsive");

                //            contentStr = JSON.stringify(detailItems[0].detail);
                //            console.log(contentStr);

                console.log(detailItems[0].detail);
                //            + '<p><button onclick="sharePost">Share</button></p>');

                setTimeout(function(){
                    myScroll.refresh();
                });
            },
            error: function(json){
                $('#articleBody').text('There was an error loading data');
            }
        });
    }
//
}

function loaded () {
    myScroll = new IScroll('#wrapper',{ hScrollbar: false, vScrollbar: false });
}

document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', loaded, false);

document.addEventListener("deviceready", onClickBtnShare, false);


function onClickBtnShare(){
//    $(document).ready(function () {
//        $("#shareBtn").click(socialShare);
//    });
    $("#shareBtn").click(socialShare);
}

function socialShare(){
    alert("Executed");
    window.plugins.socialsharing.share('Canvas Test Share', null, null, 'http://mycanvasworld.com/api/canvas_api.php?method=getPostDetail&post_id=9572');
}

function shareComment(){
    window.location = 'commentFrom.html?nwsid=' + category;
}